from sklearn.metrics import pairwise_distances
import numpy as np

class SimulatedAnnealing():

    def __init__(self, TSP, delta_T=0.9):
        self.cities = TSP['cities']
        self.coords = TSP['coords']
        self.round_path = TSP.get('round_path') or False # if we return to start
        self.n = len(self.cities)
        self.D = pairwise_distances(self.coords) # Distance matrix
        self._current =  self._initialize_config() # Initial random path
        self._T = self._initialize_temperature(self._current)
        self.delta_T = delta_T


    def search(self, store_history=False):
        _current_E = self.E(self._current)
        self._T_updates = 0
        self._previous_temp_E = _current_E
        self._freeze_count = 0
        history = [_current_E] if store_history else None

        perturbations, iterations = 0, 0
        while True:
            # DEBUG
            # print(f"perturbations: {perturbations} | iterations = {iterations} | freeze count = {self._freeze_count}")
            
            # Update config
            _next = self.get_next(self._current)
            _next_E = self.E(_next)
            
            # Accept / Reject
            _delta_E = _next_E - _current_E
            P_accept = 1 if _delta_E<0 else np.exp(-_delta_E/self._T)
            if np.random.uniform() < P_accept: # Accept update
                self._current = _next
                _current_E = _next_E
                perturbations += 1

            iterations += 1
                
            if store_history:
                history.append(_current_E)
            # Equilibrium condition
            if self._equilibrium(perturbations, iterations):
                # Frozen condition
                if self._frozen(_current_E):
                    break
                else:
                    self._reduce_temperature()
        
        # TODO: return the permutation and the permuted cities list
        if store_history:
            return self._current, _current_E, history
        return self._current, _current_E

    def _frozen(self, current_E):
        freeze_limit = 3
        if current_E < self._previous_temp_E:
            # Reset count because of improvement
            self._freeze_count = 0 
            self._previous_temp_E = current_E
            return False
        else:
            self._freeze_count += 1
        
        return self._freeze_count >= freeze_limit

    def _reduce_temperature(self):
        self._T *= self.delta_T
        self._T_updates += 1

    def _equilibrium(self, p, i):
        max_p = 12 * self.n
        max_i = 100 * self.n
        return p>= max_p or i>=max_i

    def _initialize_config(self):
        config = np.random.permutation(self.n)
        if self.round_path:
            config = np.append(config, config[0])
        return config

    def _initialize_temperature(self, x0):
        neighbors = self._get_neighbors(x0)
        E0 = self.E(x0) # store to save computations
        avg_delta = 0
        for i in range(100):
            neighbor = self._random_neighbor(neighbors)
            delta_E = E0 - self.E(neighbor) # this step can be further optimized by pre-computing
            avg_delta += delta_E
        avg_delta = np.abs(avg_delta/100)
        T0 = -avg_delta / np.log(0.5)
        return T0

    def get_next(self, x):
        return self._random_neighbor(self._get_neighbors(x))

    def _random_neighbor(self, neighbors):
        idx = np.random.choice(np.arange(len(neighbors))) # choose a random neighbor
        return neighbors[idx]

    def E(self, x) -> float:
        cost = 0
        for i in range(len(x)-1):
            cost += self.D[x[i], x[i+1]]
        return cost

    def _get_neighbors(self, x) -> list:
        '''Return list of available 2-swaps from a given permutation x.'''
        neighbors = []
        # note: self.n = len(x) - 1, we neglect the final element which just fills the loop
        for i in range(self.n):
            for j in range(i+1, self.n):
                neighbor = x.copy()
                neighbor[i], neighbor[j] = x[j], x[i] # 2-swap
                if i == 0: 
                    # We append the newly switch element at the end of the list
                    # This is to fill the loop
                    neighbor[-1] = neighbor[0]
                neighbors.append(neighbor)
        return neighbors


class GreedySearch():
    def __init__(self, TSP):
        self.cities = TSP['cities']
        self.coords = TSP['coords']
        self.round_path = TSP.get('round_path') or False # if we return to start
        self.n = len(self.cities)
        self.D = pairwise_distances(self.coords) # Distance matrix
        self._current =  np.random.choice(self.n) # Initial random city
        self._unvisited = set(range(self.n))
        self._unvisited.remove(self._current)

    def search(self):
        path = [self._current]
        while len(self._unvisited) > 0:
            self._current = self.get_next(self._current)
            path.append(self._current)
        path.append(path[0]) # form closed loop
        return path, self.E(path)

    def get_next(self, x):
        '''Return the nearest neighbor of x which is yet unvisited.'''
        best_distance = np.inf
        best = None
        for c in self._unvisited:
            dist = self.D[x, c]
            if dist < best_distance:
                best_distance = dist
                best = c
        self._unvisited.remove(best)
        return best
        
    def E(self, x) -> float:
        '''Compute energy of configuration x (list)'''
        cost = 0
        for i in range(len(x)-1):
            cost += self.D[x[i], x[i+1]]
        return cost
        

def read_data(file):
    '''Return coordinates of cities in a dict from a .dat file'''
    with open(file, 'r') as f:
        buffer = f.read()
        rows = buffer.split('\n')
        rows.remove('')
        coordinates = {row.split()[0] : (float(row.split()[1]), float(row.split()[2])) for row in rows}

    return coordinates

def extract_coords(coords_dict):
    '''Return a list of city names and a numpy array of corresponding coordinates'''
    cities = list(coords_dict.keys())
    coords = np.array([[x,y] for x,y in coords_dict.values()])
    return cities, coords



# Test from script
if __name__ == '__main__':
    import sys
    filename = sys.argv[1]
    coords_dict = read_data(filename)
    cities, coords = extract_coords(coords_dict)

    TSP = {
        'cities': cities,
        'coords': coords,
        'round_path': True
    }

    sa = SimulatedAnnealing(TSP)
    print('Initial config: ', sa._current)
    print(f'Initial energy: {sa.E(sa._current):.2f}')
    print(f'Initial temperature: {sa._T:.2f}')

    config, energy = sa.search()

    print('Final config: ', config)
    print(f'Final energy: {energy:.2f}')
    # Convert permutation into list of cities
    path = [cities[c] for c in config]
    print('Path: ', path)