'''
This module contains the implementation of the Tabu Search algorithm applied to the 
Quadratic Assignment problem.

It implements the TabuSearch object with it's associated TabuList memory structure.
'''

import numpy as np
from tabu_search import TabuList, TabuSearch

class TabuMatrix(TabuList):
    def __init__(self, n: int, tenure: int=1, diversification=True, u=None) -> None:
        super().__init__(tenure=tenure)
        self.n = n
        self.tabu_matrix = np.zeros((n,n), dtype=int)
        self.diversification = diversification
        if diversification:
            self.u = u if u is not None else n**2
            self.placements = self.tabu_matrix.copy() # stores the last placement of a facility in a given position


    def add(self, node, t: int) -> None:
        x0 = node[0]
        x1 = node[1]
        # To see what the 2-swap was, subtract both vectors and see which elements are non equal to 0
        # E.g: x0 ([2,3,1,0]) - x1 ([1,3,2,0]) = [1,0,-1,0]
        # So facility 2 was removed from position 0, and facility 1 from position 2
        # ---> insert t+tenure inside the tabu matrix in positions [0,2] and [2,1]
        removed_positions = np.where((x0-x1)!=0)[0]
        removed_facilities = [ x0[removed_positions[0]], x0[removed_positions[1]] ]

        for pos, fac in zip(removed_positions, removed_facilities):
            self.tabu_matrix[pos][fac] = t + self.tenure

    def is_tabu(self, node, t: int) -> None:
        x0 = node[0]
        x1 = node[1]
        # Check if movement is tabu, by checking if the values in the tabu matrix
        # associated to the positions of facililities removed in the 2-swap
        # are both greater than the current iteration t

        # To see what the 2-swap was, subtract both vectors and see which elements are non equal to 0
        # E.g: x0 ([2,3,1,0]) - x1 ([1,3,2,0]) = [1,0,-1,0]
        # So facility 2 was removed from position 0, and facility 1 from position 2
        # ---> insert t+tenure inside the tabu matrix in positions [0,2] and [2,1]
        removed_positions = np.where((x0-x1)!=0)[0]
        removed_facilities = [ x0[removed_positions[0]], x0[removed_positions[1]] ]

        for pos, fac in zip(removed_positions, removed_facilities):
            if self.tabu_matrix[pos][fac] < t:
                return False
        return True
    
    def diversify(self, current, neighbors: list, t: int) -> list:
        # Filter out neighbors by only selecting those where the corresponding move is allowed.
        # A move is allowed if one of its 2 new added placements has not occured in the last u iterations
        # This is verified by looking in the placements matrix for any (r,i) that has a value smaller than t-u
        
        # Get the placements older than u iterations
        old_placements = np.where(self.placements<(t-self.u))
        old_placements = [o for o in zip(old_placements[0], old_placements[1])] # ex. [(0, 0), (0, 1), (0, 2), (1, 0), (1, 2), (1, 3)...]

        if len(old_placements)>0:
            x0 = current
            allowed_neighbors = []
            for neighbor in neighbors:
                x1 = neighbor
                # Same logic as in add() and is_tabu() to get extract added positions from a move
                added_positions = np.where((x0-x1)!=0)[0]
                added_facilities = [ x1[added_positions[0]], x1[added_positions[1]] ]

                old = False
                for pos, fac in zip(added_positions, added_facilities):
                    if (pos, fac) in old_placements:
                        old = True
                if old:
                    allowed_neighbors.append(neighbor)

            # Manage case when allowed nodes is empty
            # In this case we force a new node to have a random old placement
            if len(allowed_neighbors) == 0:
                new_id = np.random.choice(np.arange(len(old_placements)))
                r, i = old_placements[new_id]
                allowed_neighbors.append(self._swap(x0), r, i)
                
            return allowed_neighbors
        else:
            return neighbors
        
    @staticmethod
    def _swap(x, r, i):
        # add facility i to position r in permutation x, and swap accordingly
        i_ = x[r] # facility to swap with
        r_ = x.index(i) # position to swap with
        new = x.copy()
        # swap
        new[r], new[r_] = i, i_
        return new
        
    def get_value(self):
        return self.tabu_matrix

class QAPTabuSearch(TabuSearch):
    goal = 'min'
    def __init__(self, n: int, D: np.ndarray, W: np.ndarray, tenure: int=1, diversification=False, u=None, max_t: int=20000) -> None:
                # Generate random x0
        super().__init__(max_t=max_t)
        if D.shape != W.shape or D.shape != (n,n):
            raise ValueError(f'Incompatible input shapes for D, W with n={n}, got {D.shape} and {W.shape} respectively.')
        self.n = n
        self.D = D
        self.W = W
        self._current = np.random.permutation(self.n)
        self.tabu_list = TabuMatrix(n, tenure=tenure, diversification=diversification, u=u)

    def _get_neighbors(self, x) -> list:
        neighbors = []
        for i in range(self.n):
            for j in range(i+1, self.n):
                neighbor = x.copy()
                neighbor[i], neighbor[j] = x[j], x[i] # 2-swap
                neighbors.append(neighbor)
        return neighbors

    def objective_function(self, x) -> int:
        # Note: This operation can be optimised if we can somehow
        # find the operation to permute D's indeces.
        cost = 0
        for i in range(self.n):
            for j in range(self.n):
                cost += self.W[i,j]*self.D[x[i],x[j]]
        return cost