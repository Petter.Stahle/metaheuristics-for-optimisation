'''
This module contains the abstract classes for which act as a boilerplate for the Tabu Search algorithm.
'''

import numpy as np

class TabuList():

    def __init__(self, tenure: int=1, diversification=False, u=None) -> None:
        # Generate the empty tabu list
        self.tenure = tenure

    def add(self, node: tuple, current_iteration: int) -> None:
        # Add movement "x0 -> x1" to tabu list
        pass

    def is_tabu(self, node: tuple, current_iteration: int) -> bool:
        # Return whether a movement "xi -> xj" is tabu or not
        pass

    def remove(self, node: tuple) -> None:
        # Used in combination with the long-term memory, which allows an element to be removed from
        # the tabu list if certain conditions are met.
        pass

    def get_value(self):
        pass

    def diversify(self):
        pass

class TabuSearch():

    goal = None # Goal of the objective function (either min or max)

    def __init__(self, diversification=False, u=None, max_t: int=20000) -> None:
        # Generate random x0
        # Assign empty tabu list
        # self.tabu_list = self.tabu_list_impl()
        # Initialize iteration 0
        self.max_t = max_t
        self.diversification = diversification
        if diversification:
            self.u = u 
        self._current, self._next = None, None
        self.tabu_list = None
        
    def search(self, debug=False) -> tuple:
        best_el = self._current
        best_score = self.objective_function(self._current)
        path = [(best_el, best_score)]
        for t in range(1, self.max_t+1):
            if debug:
                print("Iteration - ", t)
                print("Current: ", self._current)
                print("Best score: ", best_score)
                print("Tabu list: \n", self.tabu_list.get_value())
                print("----------------")
            # Select the best non-tabu neighbor
            neighbors = self._get_neighbors(self._current)
            self._next, score = self._get_next(self._current, neighbors, t)
            # Update best solution so far
            if self._is_better(score, best_score):
                best_el, best_score = self._next, score
            # Update tabu list
            self._update_tabu_list(t)
            # Update current node
            self._current = self._next
            path.append((self._next, score))

        return best_el, best_score, path

    def _update_tabu_list(self, t: int) -> None:
        self.tabu_list.add((self._current, self._next), t)

    def _get_neighbors(self, element) -> list:
        # To implement
        pass

    def _get_next(self, element, neighbors: list, t: int) -> tuple:
        # Select best of non-tabu movements
        allowed_neighbors = []
        for neighbor in neighbors:
            if not self.tabu_list.is_tabu((element, neighbor), t):
                allowed_neighbors.append(neighbor)

        # Add diversification
        if self.diversification:
            allowed_neighbors = self.tabu_list.diversify(element, allowed_neighbors, t)

        next_neighbor, score = self._find_optimal(allowed_neighbors)
        return next_neighbor, score

        
    def objective_function(self, element) -> int:
        # To implement
        pass

    def _is_better(self, score_1, score_2) -> bool:
        # Return whether score 1 is better than score 2
        return score_1<=score_2 if self.goal=='min' else score_1>=score_2

    def _find_optimal(self, elements):
        scores = np.empty(len(elements))
        for i, el in enumerate(elements):
            score = self.objective_function(el)
            scores[i] = score
        # minimize or optimize objective function
        id_best = np.argmin(scores) if self.goal=='min' else np.argmax(scores)
        best_score = scores[id_best]
        best_el = elements[id_best]

        return best_el, best_score