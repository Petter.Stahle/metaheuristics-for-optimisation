import numpy as np
from sklearn.metrics import mean_absolute_error as MAE, \
    mean_squared_error as MSE, accuracy_score

from nn import NeuralNet

NN_CONFIG = {
    'layers'                : (400,25,1),
    'kernel_norm_constraint': 0.5,
    'activation_function'   : 'sigmoid',
    'loss'                  : 'MSE'
}

class PSO:

    def __init__(self, n, inertia=0.9, local_acc=2, global_acc=2, vmax=0.5, model_config=NN_CONFIG) -> None:
        self.n = n

        params = {
            'inertia'   : inertia, 
            'local_acc' : local_acc, 
            'global_acc': global_acc,
            'vmax'      : vmax
            }

        self.model_config = model_config
        self.particles = []
        for i in range(n):
            m = NeuralNet(**model_config)
            self.particles.append(Particle(params, m))

        self.global_best_weights = None
        self.global_best_loss = np.inf

    def fit(self, X, y, steps=100, track_losses=False, verbose=False):

        losses = np.zeros((steps+1, self.n)) if track_losses else None

        for i, p in enumerate(self.particles):
            local_loss = p.get_loss(X, y)
            if local_loss < self.global_best_loss:
                self.global_best_loss = local_loss
                self.global_best_weights = p.best_weights
            if track_losses:
                losses[0,i] = local_loss

        if verbose:
            print(f'PSO -- Initial best loss = {self.global_best_loss:.3f}')

        for t in range(steps):
            for i, p in enumerate(self.particles):
                local_loss = p.step(X, y, self.global_best_weights)

                if local_loss < self.global_best_loss:
                    self.global_best_loss = local_loss
                    self.global_best_weights = p.best_weights
                if track_losses:
                    losses[t+1,i] = local_loss
        
            if verbose:
                print(f'Step {t+1} -- best loss = {self.global_best_loss:.3f}')
        
        return losses

    def get_best_model(self):
        best_model = NeuralNet(**self.model_config)
        best_model.set_weights(self.global_best_weights)
        return best_model

class Particle:
    
    def __init__(self, params, model) -> None:
        self.params = params
        self.model = model
        self.best_weights = self.model.weights
        self.best_loss = np.inf
        self._init_velocities()
        
    def _init_velocities(self):
        scale = self.params['vmax'] if self.params['vmax'] else 1
        self.velocities = [None] * len(self.best_weights)
        for i, layer in enumerate(self.best_weights):
            self.velocities[i] = np.random.normal(scale=scale, size=layer.shape)

    def get_loss(self, X, y, update=True):
        local_loss = self.model.evaluate(X, y)
        if local_loss < self.best_loss and update:
            self.best_loss = local_loss
            self.best_weights = self.model.weights
        return local_loss

    def _update_velocities(self, global_best_weights):
        # v = wv + c1r1(b-s) + c2r2(G-s)
        new_velocities = [None] * len(self.best_weights)
        weights = self.model.weights
        r1, r2 = np.random.random(), np.random.random()

        for i, layer in enumerate(weights):
            new_v = self.params['inertia'] * self.velocities[i] \
                +   self.params['local_acc'] * r1 * (self.best_weights[i] - layer) \
                +   self.params['global_acc'] * r2 * (global_best_weights[i] - layer)
            # apply vmax cutoff
            new_v[new_v>self.params['vmax']] = self.params['vmax']
            new_velocities[i] = new_v
        self.velocities = new_velocities

    def _update_weights(self):
        old_weights = self.model.weights
        new_weights = [None] * len(self.best_weights)
        for i, layer in enumerate(old_weights):
            new_w = layer + self.velocities[i]
            new_w = self._handle_constraints(new_w, i)
            new_weights[i] = new_w
        self.model.set_weights(new_weights)

    def _handle_constraints(self, weights, layer):
        # weights that have violated constraints will be set to 
        # nearest limit (l1 norm-wise in nearest coordinate)
        mask = (weights>self.model.constraint)
        weights[mask] = self.model.constraint
        # We can reset the associated velocity to 0 since direction
        # is no good
        self.velocities[layer][mask] = 0
        return weights

    def step(self, X, y, global_best_weights):
        self._update_velocities(global_best_weights)
        self._update_weights()
        return self.get_loss(X, y)
    


# Helper function to load example MNIST digits 2/3
def load_data(shuffle=True):
    X = np.loadtxt('data/X.dat', delimiter=',')
    y = np.loadtxt('data/Y.dat', dtype=int)
    if shuffle:
        idx = np.arange(X.shape[0])
        np.random.shuffle(idx)
        X = X[idx]
        y = y[idx]
    return X,y

if __name__ == '__main__':
    X, y = load_data()
    n = 25
    pso = PSO(n)
    losses = pso.fit(X, y, steps=10, track_losses=True, verbose=True)