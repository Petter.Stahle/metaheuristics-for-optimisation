import numpy as np
from sklearn.metrics import mean_absolute_error as MAE, \
    mean_squared_error as MSE, accuracy_score

class NeuralNet:

    losses = {
        'MSE': MSE,
        'MAE': MAE
    }

    def __init__(self, layers=(400,25,1), kernel_norm_constraint=None,
                activation_function='sigmoid', loss='MSE') -> None:
        self.constraint = kernel_norm_constraint
        self._init_weights(layers)
        if activation_function not in ['sigmoid', 'relu']:
            raise KeyError(
                'Wrong activation function specified, got ', 
                activation_function
                )
        activation_functions = {
        'sigmoid' : self.sigmoid,
        'relu': self.relu
        }
        self.activation = activation_functions[activation_function]
        if loss not in ['MSE', 'MAE']:
            raise NotImplementedError(
                'Wrong loss function specified, got ', 
                loss
                )
        self.loss = self.losses[loss]

    def _init_weights(self, layers):
        self.weights = []
        for i in range(1, len(layers)):
            layer = np.random.normal(
                0, 
                self.constraint if self.constraint else 1,
                size = (layers[i], layers[i-1]+1) # add bias
                )
            self.weights.append(layer)
        
    def set_weights(self, weights):
        self.weights = weights

    def evaluate(self, X, y):
        y_pred = self.predict(X)
        return self.loss(y, y_pred)

    def forward(self, X):
        # input layer, 
        V = X.copy().T
        for theta in self.weights:
            # augment V
            V = np.concatenate([np.ones((1,V.shape[1])),V], axis=0)
            V = self.activation(theta @ V)
        
        return V.T

    def predict(self, X):
        y = self.forward(X)
        # this is for binary classification, for multi-class, use argmax
        # where y is in one-hot encoding
        preds = np.zeros(y.shape)
        preds[y>0.5] = 1
        return preds

    @staticmethod
    def sigmoid(Z):
        return (1+np.exp(-Z))**(-1)

    @staticmethod
    def relu(Z):
        return np.maximum(0,Z)