import random
import numpy as np
from typing import Optional
import copy

# This is the machine on which programs are executed
# The output is the value on top of the pile. 
class CPU:
    def __init__(self):
        self.pile=[]
    
    def reset(self):
        while len(self.pile)>0:self.pile.pop()

class StackError(Exception):
    def __init__(self, message):
        super().__init__(message)
        

# These are the instructions
def AND(cpu, data):
    cpu.pile.pop() # consume term
    # Check if there are enough operands
    if not len(cpu.pile) >= 2:
        raise StackError(f'Not enough operands for AND -- stack: {cpu.pile}')
    v1, v2 = cpu.pile.pop(), cpu.pile.pop()
    if not (isinstance(v1, int) or isinstance(v2, int)):
        raise StackError(f'Invalid operands for AND, got: [{v1}, {v2}] ')
    
    res = v1 and v2
    cpu.pile.append(res)

def OR(cpu, data):
    cpu.pile.pop() # consume term
    # Check if there are enough operands
    if not len(cpu.pile) >= 2:
        raise StackError(f'Not enough operands for OR -- stack: {cpu.pile}')
    v1, v2 = cpu.pile.pop(), cpu.pile.pop()
    if not (isinstance(v1, int) or isinstance(v2, int)):
        raise StackError(f'Invalid operands for OR, got: [{v1}, {v2}] ')
    
    res = v1 or v2
    cpu.pile.append(res)

def XOR(cpu, data):
    cpu.pile.pop() # consume term
    # Check if there are enough operands
    if not len(cpu.pile) >= 2:
        raise StackError(f'Not enough operands for XOR -- stack: {cpu.pile}')
    v1, v2 = cpu.pile.pop(), cpu.pile.pop()
    if not (isinstance(v1, int) or isinstance(v2, int)):
        raise StackError(f'Invalid operands for XOR, got: [{v1}, {v2}] ')
    
    res = v1 ^ v2
    cpu.pile.append(res)

def NOT(cpu, data):
    cpu.pile.pop() # consume term
    # Check if there are enough operands
    if not len(cpu.pile) >= 1:
        raise StackError(f'Not enough operands for NOT -- stack: {cpu.pile}')
    v1 = cpu.pile.pop()
    if not (isinstance(v1, int)):
        raise StackError(f'Invalid operand for NOT, got: [{v1}] ')
    
    res = int(not v1)
    cpu.pile.append(res)

def NAND(cpu, data):
    # Try this to see if we can solve the problem
    # NAND = Not And
    # How to do it with the stack?
    cpu.pile.pop() # consume term
    # Check if there are enough operands
    if not len(cpu.pile) >= 2:
        raise StackError(f'Not enough operands for NAND -- stack: {cpu.pile}')
    v1, v2 = cpu.pile.pop(), cpu.pile.pop()
    if not (isinstance(v1, int) or isinstance(v2, int)):
        raise StackError(f'Invalid operands for NAND, got: [{v1}, {v2}] ')
    
    res = not (v1 and v2)
    cpu.pile.append(res)

# Push values of variables on the stack.      
def X1(cpu, data):
    cpu.pile.pop() # consume term
    cpu.pile.append(data[0])

def X2(cpu, data):
    cpu.pile.pop() # consume term
    cpu.pile.append(data[1])

def X3(cpu, data):
    cpu.pile.pop() # consume term
    cpu.pile.append(data[2])

def X4(cpu, data):
    cpu.pile.pop() # consume term
    cpu.pile.append(data[3])

# Execute a program
def execute(program: list, cpu: CPU, data: list, verbose: bool=False) -> tuple:
    ''' Returns result of program execution, and the length of the valid 
    subset of the program (for cases where the program is completely,
    invalid, result is None)
    '''

    if verbose: print('Data: ', data)
    _actions = {
        'AND': AND,
        'OR' : OR,
        'XOR': XOR,
        'NOT': NOT,
        'NAND': NAND,
        'X1' : X1,
        'X2' : X2,
        'X3' : X3,
        'X4' : X4
    }

    cpu.reset()
    if verbose: print('Exec program: ', program)

    for i, term in enumerate(program):
        cpu.pile.append(term)
        try:
            _actions[cpu.pile[-1]](cpu, data)
        except StackError as e:
            if verbose: print(e)
            if len(cpu.pile)>0 and isinstance(cpu.pile[-1], int):
                return (cpu.pile[-1], i)
            else: return (None, 0)
            
        if verbose: 
            print(term)
            print(cpu.pile)

    if verbose: print('End execution')
    if len(cpu.pile) != 1:
        if verbose:
            print(
                'Error: End of program didn\'t get a correct result ' \
                    '-- stack: ', cpu.pile)
        
    result = cpu.pile.pop()
    if verbose: print('Result: ', result)
    return (result, len(program))

# Generate a random program
def randomProg(length: int,functionSet: list, terminalSet: list) -> list:
    # Uniformly sample from the union of functionset and terminalset
    prog = [random.choice(terminalSet)] # first term can't be an operator in post-fix!
    for i in range(length-1):
        term = random.choice(functionSet + terminalSet)
        prog.append(term)
    return prog

# Computes the fitness of a program. 
# The fitness counts how many instances of data in dataset are correctly computed by the program
def computeFitness(prog: list, cpu: CPU, dataset: list) -> int: 
    fitness = 0
    for data in dataset:
        res, pos = execute(prog, cpu, data)
        fitness += int(res==data[-1])
    return fitness, pos
    
def get_fitnesses(population: list, cpu: CPU, dataset: list) -> list:
    fitnesses=[]
    lengths = []
    for i in range(len(population)):
        prog=population[i]
        f, pos = computeFitness(prog, cpu, dataset)
        fitnesses.append(f)
        lengths.append(pos)
    return fitnesses, lengths


# Selection using 2-tournament.
def selection(population, cpu, fitnesses):
    # TODO: implement k-tournament 

    new_population=[]
    n=len(population)
    for i in range(n):    
        i1=random.randint(0,n-1)
        i2=random.randint(0,n-1)
        if fitnesses[i1]>fitnesses[i2]:
            new_population.append(population[i1])
        else:
            new_population.append(population[i2])
    return new_population

def crossover(population,p_c):
    new_population=[]
    n=len(population)
    i=0
    while(i<n):
        p1=population[i]
        p2=population[(i+1)%n]
        m=len(p1)
        if random.random()<p_c:  # crossover
            k=random.randint(1,m-1)
            newP1=p1[0:k]+p2[k:m]
            newP2=p2[0:k]+p1[k:m]
            p1=newP1
            p2=newP2
        new_population.append(p1)
        new_population.append(p2)
        i+=2
    return new_population

def mutation(population,p_m,terminalSet,functionSet):
    new_population=[]
    nT=len(terminalSet)-1
    nF=len(functionSet)-1
    for p in population:
        for i in range(len(p)):
            if random.random()>p_m:continue
            if random.random()<0.5: 
                p[i]=terminalSet[random.randint(0,nT)]
            else:
                p[i]=functionSet[random.randint(0,nF)]
        new_population.append(p)
    return new_population


def solve(pop_size: int, max_gen: int=100, p_c: float=0.1, p_m: float=0.1,
    prog_length: int=5, terminal_set: list=None, function_set: list=None, 
    dataset: list=None, patience: Optional[int]=None, 
    track_history: bool=False) -> tuple:
    '''Solve the boolean look-up table using genetic programming.

        Parameters
        ----------
        pop_size : int
        max_gen : int, optional
            Max iteration count, by default 100
        p_c : float, optional
            Crossover probability, by default 0.1
        p_m: float, optional
            Mutation probability, by default 0.1
        prog_length: int, optional
            Maximum program length, by default 5
        terminal_set: list, optional
            Provide an alternate terminal set other than 
            ["X1", "X2","X3", "X4"]
        function_set: list, optional
            Provide an alternate function set other than 
            ["AND", "OR", "NOT", "XOR"]
        dataset: list, optional
            Provide an alternate look-up table other than the default
        patience : int, optional
            Number of consecutive iterations without improvement for 
            early stopping, by default None
            If None, then no early stopping
        track_history: bool, optional
            Keep track of the evolution of the population and fitness

        Returns
        -------
        tuple
            programs, fitnesses, lengths (of valid subset of program) 
            + history if track_history is True
        '''

    terminal_set = terminal_set or ["X1", "X2","X3", "X4"]
    function_set = function_set or ["AND", "OR", "NOT", "XOR"]
    dataset = dataset or \
        [[0,0,0,0,0],[0,0,0,1,1],[0,0,1,0,0],[0,0,1,1,0],
         [0,1,0,0,0],[0,1,0,1,0],[0,1,1,0,0],[0,1,1,1,1],
         [1,0,0,0,0],[1,0,0,1,1],[1,0,1,0,0],[1,0,1,1,0],
         [1,1,0,0,0],[1,1,0,1,0],[1,1,1,0,0],[1,1,1,1,0]]
    cpu = CPU()
    stagnation_count = 0

    # Generate the initial population
    pop = [randomProg(prog_length, function_set, terminal_set) \
            for _ in range(pop_size)]

    
    fitnesses, lengths = get_fitnesses(pop, cpu, dataset)
    best_child = np.argmax(fitnesses)
    best_fitness = fitnesses[best_child] 
    best_prog = (copy.deepcopy(pop[best_child]), best_fitness, lengths[best_child]) # (prog, fitness, length)

    if track_history:
        history = {
            'population': [copy.deepcopy(pop)],
            'fitnesses': [copy.deepcopy(fitnesses)], 
            'lengths': [copy.deepcopy(lengths)],
            'best_prog': [copy.deepcopy(best_prog)] # 3-tuple of (prog, fitness, length)
        }

    # Evolution. Loop on the creation of population at generation i+1 from population at generation i, through selection, crossover and mutation.
    for t in range(max_gen):

        # Selection
        pop = selection(pop, cpu, fitnesses)

        # print('after selection')
        # print(pop)

        # Crossover
        pop = crossover(pop, p_c)
        # print('after crossover')
        # print(pop)

        # Mutation
        pop = mutation(pop, p_m, terminal_set, function_set)
        # print('after mutation')
        # print(pop)
        
        new_fitnesses, lengths = get_fitnesses(pop, cpu, dataset)
        best_child = np.argmax(new_fitnesses)
        if best_fitness < new_fitnesses[best_child]: # Improvement
            best_fitness = new_fitnesses[best_child]
            best_prog = (copy.deepcopy(pop[best_child]), best_fitness, lengths[best_child])
            stagnation_count = 0
        else: 
            stagnation_count += 1
            if not best_prog[0] in pop: # if previous best is not there
                # Elitism: replace current worst with previous best
                worst_child = np.argmin(new_fitnesses)
                pop[worst_child] = copy.deepcopy(best_prog[0])
                new_fitnesses[worst_child] = best_fitness
                lengths[worst_child] = best_prog[2]

        fitnesses = copy.deepcopy(new_fitnesses)

        if track_history:
            history['population'].append(copy.deepcopy(pop))
            history['fitnesses'].append(copy.deepcopy(fitnesses))
            history['lengths'].append(copy.deepcopy(lengths))
            history['best_prog'].append(copy.deepcopy(best_prog))

        if patience and stagnation_count >= patience:
            break


    return (pop, fitnesses, lengths, history) if track_history \
        else (pop, fitnesses, lengths)

def print_program(program: list, fitness: int, position: int) -> None:
    print(f'Program: {program[:position]}')
    print(f'Fitness: {fitness}')

#-------------------------------------

if __name__ == '__main__':

    # LOOK-UP TABLE YOU HAVE TO REPRODUCE.
    nbVar = 4
    dataset=[[0,0,0,0,0],[0,0,0,1,1],[0,0,1,0,0],[0,0,1,1,0],[0,1,0,0,0],[0,1,0,1,0],[0,1,1,0,0],[0,1,1,1,1],[1,0,0,0,0],[1,0,0,1,1],[1,0,1,0,0],[1,0,1,1,0],[1,1,0,0,0],[1,1,0,1,0],[1,1,1,0,0],[1,1,1,1,0]]

    cpu=CPU()

    # Function and terminal sets.
    functionSet=["AND", "OR", "NOT", "XOR"]
    terminalSet=["X1", "X2","X3", "X4"]

    # Example of program.
    # prog=["X1", "X2", "AND", "X3", "OR"]
    progLength = 5
    # prog=randomProg(progLength,functionSet,terminalSet)
    # print(prog)

    # Execute a program on one row of the data set.
    # data = dataset[0]
    # output=execute(prog,cpu,data)
    # print (output)
    # print ("-------------")

    # Parameters
    pop_size = 10
    p_c = 0.1
    p_m = 0.1
    generations = 1000
    (pop, fitnesses, lengths, history) = solve(pop_size, generations, p_c, p_m, track_history=True)
