import numpy as np

from typing import Optional, Union


class GA:

    def __init__(self, n: int, m: int=10, a: int=10, b: int=1000, 
                seed: int=None) -> None:
        '''Genetic Algorithm for optimising the 2-dim function f.

        Parameters
        ----------
        n : int
            Pop size
        m : int, optional
            Gene length, by default 10
        a : int, optional
            Search space lowerbound, by default 10
        b : int, optional
            Search space upperbound, by default 1000
        seed: int, optional
            Numpy seed for random population init, by default None
        '''

        self.n = n
        self.m = m
        self.a = a
        self.b = b
        self.seed = seed
        

    def solve(self, max_gen: int=1000, patience: Optional[int]=None, 
            pc: float=0.6, pm: float=0.01, track=False) -> tuple:
        '''Optimize f.

        Parameters
        ----------
        max_gen : int, optional
            Max iteration count, by default 1000
        patience : int, optional
            Number of consecutive iterations without improvement for 
            early stopping, by default None
            If None, then no early stopping
        pc : float, optional
            crossover probability, by default 0.6
        pm: float, optional
            mutation (bitwise flip) probability, by default 0.01

        Returns
        -------
        tuple
            best coordinates, best fitness
        '''
        # Initialize population
        self.initial_pop = self.population = self._init_pop(self.seed)
        self.best_coordinates, self.best_fitness = (None, None), np.inf
        stagnation_count = 0
        
        if track:
            self.history = {
                'population': [self.decode_pop()],
                'fitnesses': [self.get_fitnesses()]
            }

        for t in range(max_gen):

            fitnesses = self.get_fitnesses()
            pop = self.selection(fitnesses, n=self.n)
            if pc > 0:
                pop = self.crossover(pop, pc=pc)
            pop = self.mutation(pop, pm=pm)
            
            new_fitnesses = self.get_fitnesses(pop)
            best_child = np.argmin(new_fitnesses)
            if new_fitnesses[best_child] < self.best_fitness: # Improvement
                stagnation_count = 0
                self.best_fitness = new_fitnesses[best_child]
                self.best_coordinates = self.decode(pop[best_child])
            else: # Elitism
                stagnation_count += 1
                worst_child = np.argmax(new_fitnesses)
                best_parent = np.argmin(fitnesses)
                pop[worst_child] = self.population[best_parent]

            self.population = pop

            if track:
                self.history['population'].append(self.decode_pop())
                self.history['fitnesses'].append(self.get_fitnesses())

            if patience and stagnation_count >= patience:
                break

        self.n_gen = t+1
        return self.best_coordinates, self.best_fitness

    def mutation(self, pop, pm=0.01):
        mask = (np.random.random(pop.shape) < pm)
        pop ^= mask
        return pop
    
    def crossover(self, parents, pc=0.6):
        '''Sequentially breed parents and apply crossover with 
        probability `pc`. '''
        
        children = np.empty_like(parents)
        for i in range(self.n//2):
            
            p1, p2 = parents[2*i], parents[2*i+1]
            c1, c2 = p1.copy(), p2.copy()
            
            if np.random.random() < pc: # Crossover
                # p = [x1 x2 y1 y2]
                # x2 = [5:10]
                # y2 = [15:]
                # c1_x2 = p2_x2
                c1[self.m//2:self.m] = p2[self.m//2:self.m]
                # c1_y2 = p2_y2
                c1[self.m+self.m//2:] = p2[self.m+self.m//2:]
                # c2_x2 = p1_x2
                c2[self.m//2:self.m] = p1[self.m//2:self.m]
                # c2_y2 = p1_y2
                c2[self.m+self.m//2:] = p1[self.m+self.m//2:]
     
            children[2*i] = c1
            children[2*i+1] = c2

        return children

    def selection(self, fitnesses, n=1):
        '''Return indeces of n selected individuals using 5 - tournament 
        selection given the list of fitnesses.'''
        selection_method = self._tournament
        selections = []

        for _ in range(n):
            selections.append(selection_method(fitnesses))
        return self.population[selections]

    @staticmethod
    def _tournament(fitnesses, k=5):
        idx = np.arange(len(fitnesses))
        samples = np.random.choice(idx, k)
        best_sample_ind = np.argmin(fitnesses[samples])
        return samples[best_sample_ind]

    def _init_pop(self, seed=None) -> np.ndarray:
        pop = np.empty((self.n, 2*self.m), dtype=int)
        if not seed is None:
            random_state = np.random.RandomState(seed=seed)
            pop = random_state.randint(2, size=(self.n, 2*self.m))
        else:
            pop = np.random.randint(2, size=(self.n, 2*self.m))
        return pop

    def decode_pop(self, pop: Optional[np.ndarray]=None) -> np.ndarray:
        '''Decode population's binary representation into array of
        coordinates of size (n, 2).

        Parameters
        ----------
        pop : Optional[np.ndarray], optional
            Bit array of size (n, 2*m), by default None
            If None, then self.population is decoded.

        Returns
        -------
        np.ndarray
            (x,y) coordinate array of shape (n,2)
        '''

        pop = self.population if pop is None else pop
        bx = pop[:, :self.m]
        by = pop[:, self.m:]
        x = self._map(bx)
        y = self._map(by)

        return np.array([x, y]).T

    def decode(self, bits: np.ndarray) -> tuple:
        '''Decode an individual's binary code into coordinates.

        Parameters
        ----------
        bits : np.ndarray
            Bit array

        Returns
        -------
        tuple
            (x,y) coordinates
        '''
        bx = bits[:self.m]
        by = bits[self.m:]

        x = self._map(bx)
        y = self._map(by)

        return int(x), int(y)

    def _map(self, bits: np.ndarray) -> Union[int, np.ndarray]:
        '''Map gene bit-array `bits` to integers. '''
        base = np.array([2**i for i in range(self.m-1, -1, -1)]).T
        x = np.dot(bits, base)

        N = (x/2**self.m) * (self.b - self.a) + self.a
        if type(N) is float:
            return int(N)
        else:
            return N.astype(int)

    def get_fitnesses(self, pop: Optional[np.ndarray]=None) -> np.ndarray:
        '''Compute the fitnesses of the population.'''
        coords = self.decode_pop(pop) # if pop is None, uses self.population
        return self.fitness(coords[:,0], coords[:,1])

    @staticmethod
    def fitness(x, y):
        return (
            - np.abs(0.5 * x * np.sin( np.sqrt(np.abs(x)) ))
            - np.abs(y * np.sin( 30 * np.sqrt(np.abs(x/y)) ))
        )

    


if __name__ == '__main__':

    ga = GA(n=100)
    coords, fitness = ga.solve(max_gen=10)
    initial_coords = ga.decode_pop(ga.initial_pop)
    
    print(coords)
    print(fitness)
    # x = y = np.arange(10, 1000)
    # X, Y = np.meshgrid(x, y)
    # Z = __f(X, Y)

    # ag = AG()
    # ag.fit()
